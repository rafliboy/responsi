package com.gmail.rafliboye;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class singup extends AppCompatActivity {
    EditText nama, email;
    RadioButton pria, wanita, unknown;
    RadioGroup group;
    CheckBox nyanyi, baca, tulis, main, seni;
    Button simpan;
    TextView dataRegistrasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        nama = findViewById(R.id.username);
        email = findViewById(R.id.email);
        pria = findViewById(R.id.radioButton8);
        wanita = findViewById(R.id.radioButton9);
        unknown = findViewById(R.id.radioButton10);
        group = findViewById(R.id.group);
        nyanyi = findViewById(R.id.checkBox4);
        baca = findViewById(R.id.checkBox5);
        tulis = findViewById(R.id.checkBox6);
        main = findViewById(R.id.checkBox7);
        seni = findViewById(R.id.checkBox8);
        simpan = findViewById(R.id.btnsimpan);
        dataRegistrasi = findViewById(R.id.textData);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] bufferNama = new byte[30];
                byte[] bufferEmail = new byte[25];
                byte[] bufferGender = new byte[20];
                byte[] bufferHobi = new byte[15];

                String jk;
                //menyalin data ke buffer
                salinData(bufferNama, nama.getText().toString());
                salinData(bufferEmail, email.getText().toString());

                if (pria.isChecked()) {
                    jk = pria.getText().toString();
                } else if (wanita.isChecked()) {
                    jk = wanita.getText().toString();
                } else if (unknown.isChecked()) {
                    jk = unknown.getText().toString();
                } else {
                    jk = unknown.getText().toString();
                }


                salinData(bufferGender, jk);
                salinData(bufferEmail, email.getText().toString());

                try {
                    //menyiapkan file di memori internal
                    FileOutputStream dataFile = openFileOutput("registrasi.dat", MODE_APPEND);
                    DataOutputStream output = new DataOutputStream(dataFile);

                    //menyimpan data
                    output.write(bufferNama);
                    output.write(bufferEmail);
                    output.write(bufferGender);
                    output.write(bufferHobi);

                    //menutup file
                    dataFile.close();

                    //menampilkan pesan jika data tersimpan
                    Toast.makeText(getBaseContext(), "Data telah disimpan",
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Toast.makeText(getBaseContext(), "Kesalahan: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                tampilkanData();
            }
        });
        tampilkanData();
    }

    public void salinData(byte[] buffer, String data) {
        //mengosongkan buffer
        for (int i = 0; i < buffer.length; i++)
            buffer[i] = 0;

        //menyalin data ke buffer
        for (int i = 0; i < data.length(); i++)
            buffer[i] = (byte) data.charAt(i);

    }

    public void tampilkanData() {
        try {
            // menyiapkan file untuk dibaca
            FileInputStream dataFile = openFileInput("registrasi.dat");
            DataInputStream input = new DataInputStream(dataFile);

            //menyiapkan buffer
            byte[] bufNama = new byte[30];
            byte[] bufEmail = new byte[25];
            byte[] bufGender = new byte[20];
            byte[] bufHobi = new byte[15];
            String infoData = "Data Registrasi:\n";

            //proses membaca data
            while (input.available() > 0) {
                input.read(bufNama);
                input.read(bufEmail);
                input.read(bufGender);
                input.read(bufHobi);
                String dataNama = "";
                for (int i = 0; i < bufNama.length; i++)
                    dataNama = dataNama + (char) bufNama[i];

                String dataEmail = "";
                for (int i = 0; i < bufEmail.length; i++)
                    dataEmail = dataEmail + (char) bufEmail[i];

                String dataGender = "";
                for (int i = 0; i < bufGender.length; i++)
                    dataGender = dataGender + (char) bufGender[i];

                String dataHobi = "";
                for (int i = 0; i < bufHobi.length; i++)
                    dataHobi = dataHobi + (char) bufHobi[i];


                //format menampilkan data
                infoData = infoData + " > "
                        + dataNama + "\n"
                        + dataEmail + "\n"
                        + dataGender + "\n"
                        + dataHobi + "\n";
            }

            //menampilkan data ke teks view
            dataRegistrasi.setText(infoData);
            dataFile.close();
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Kesalahan: " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }
}
