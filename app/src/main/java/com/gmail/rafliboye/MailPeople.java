package com.gmail.rafliboye;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MailPeople extends AppCompatActivity {

    ListView list;
    //untuk menampung data string array
    String[] maintitle = {"Rafli", "Tahta", "Eko"};
    String[] subtitle = {
            "rafliboye@gmail.com", "Tahtacahya@gmail.com", "eko-oktavia@gmail.com"};
    Integer[] imgid = {R.drawable.rafli, R.drawable.tahta, R.drawable.eko};
    Button signout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_people);
        MyListView adapter = new MyListView(this, maintitle, subtitle, imgid);
        list = findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) { // TODO Auto-generated method stub
                if (position == 0) {
//code untuk menampilkan toast pertama
                    Toast.makeText(getApplicationContext(), "Kontak Rafli", Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
//code untuk menampiklan toast kedua
                    Toast.makeText(getApplicationContext(), "Kontak Tahta", Toast.
                            LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(getApplicationContext(), "Kontak eko", Toast.LENGTH_SHORT).show();
                }

            }
        });


        // signout
        signout = findViewById(R.id.btnSignOut);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MailPeople.this, MainActivity.class);
                MailPeople.this.startActivity(intent);
                finish();
            }
        });

    }
}
